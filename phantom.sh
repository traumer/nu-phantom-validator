#!/bin/sh

cat testPages | while read line; do
  echo Testing http://localhost$line
  phantomjs staticCopy.js http://localhost$line > transientFile
  html-validator --file=transientFile --verbose
  rm transientFile
done
