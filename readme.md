A script that creates a static snapshot of a list of directions defined in the **testPages** file and validates them against the w3c nu checker.

Requirements:

* nodejs: [https://github.com/nodesource/distributions#installation-instructions]
* phantomjs: [http://phantomjs.org/download.html] it has to be included on the system path or changed the reference in the script **phantom.js**
* html-validator: se instala con node package manger: ```npm install html-validator-cli -g```  [https://www.npmjs.com/package/html-validator-cli]
